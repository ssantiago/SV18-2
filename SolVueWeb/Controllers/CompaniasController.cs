﻿using SolVueWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SolVueWeb.Controllers
{
    public class CompaniasController : Controller
    {
        private SolVueContext db = new SolVueContext();
        // GET: Companias
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(db.Companias.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Compania compania)
        {
            if (!ModelState.IsValid)
            {
                return View(compania);
            }
            else
            {
                var Newcompania = new Compania
                {
                    Nombre = compania.Nombre,
                    Direccion = compania.Direccion,
                    Telefono = compania.Telefono,
                    Siglas = compania.Siglas,

                };
                db.Companias.Add(Newcompania);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                    return View(compania);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var compania = db.Companias.Find(id);
            if (compania == null)
            {
                return HttpNotFound();
            }

            return View(compania);
        }

        [HttpPost]
        public ActionResult Edit(Compania compania)
        {
            if (!ModelState.IsValid)
            {
                return View(compania);
            }
            var Newcompania = db.Companias.Find(compania.CompaniaId);
            Newcompania.Nombre = compania.Nombre;
            Newcompania.Direccion = compania.Direccion;
            Newcompania.Telefono = compania.Telefono;
            Newcompania.Siglas = compania.Siglas;

            db.Entry(Newcompania).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}