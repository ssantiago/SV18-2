﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolVueWeb.Models;
using System.IO;

namespace SolVueWeb.Controllers
{
    [Authorize(Roles ="Admin")]
    public class MatriculasController : Controller
    {
        private SolVueContext db = new SolVueContext();

        // GET: Matriculas
        public ActionResult Index()
        {
            var matriculas = db.Matriculas.Include(m => m.Modelo);
            return View(matriculas.ToList());
        }

        // GET: Matriculas/Details/5   
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Matricula matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }
            return View(matricula);
        }

        // GET: Matriculas/Create
        public ActionResult Create()
        {
            ViewBag.ModeloId = new SelectList(db.Modelos, "ModeloId", "Nombre");
            return View();
        }

        // POST: Matriculas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MatriculaView matriculaView)
        {
            if (!ModelState.IsValid)
            {
                return View(matriculaView);
            }

            //Upload Imagen
            string path = string.Empty;
            string pic = string.Empty;

            if (matriculaView.Imagen != null)
            {
                pic = Path.GetFileName(matriculaView.Imagen.FileName);
                path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);
                matriculaView.Imagen.SaveAs(path);
                using(MemoryStream ms=new MemoryStream())
                {
                    matriculaView.Imagen.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

           
            //Save Record

            var matricula = new Matricula
            {
                Siglas = matriculaView.Siglas,
                Serie = matriculaView.Serie,
                AnioFabricacion = matriculaView.AnioFabricacion,
                Poseedor = matriculaView.Poseedor,
                Capacidad = matriculaView.Capacidad,
                ModeloId = matriculaView.ModeloId,
                Certificado = matriculaView.Certificado,
                Imagen = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}", pic)

            };
            
            db.Matriculas.Add(matricula);

            try
            {
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View(matriculaView);
            }
            return RedirectToAction("Index");

            //    ViewBag.ModeloId = new SelectList(db.Modelos, "ModeloId", "Nombre", matricula.ModeloId);
            //    return View(matricula);
        }

        // GET: Matriculas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }

            var matriculaView = new MatriculaView
            {
                Siglas = matricula.Siglas,
                Serie = matricula.Serie,
                AnioFabricacion = matricula.AnioFabricacion,
                Poseedor = matricula.Poseedor,
                Capacidad = matricula.Capacidad,
                ModeloId = matricula.ModeloId,
                Certificado = matricula.Certificado,
                MatriculaId=matricula.MatriculaId
            };

            ViewBag.ModeloId = new SelectList(db.Modelos, "ModeloId", "Nombre", matricula.ModeloId);
            return View(matriculaView);
        }

        // POST: Matriculas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MatriculaView matriculaView)
        {
            if (!ModelState.IsValid)
            {
               return View(matriculaView);
            }

            //Upload Imagen
            string path = string.Empty;
            string pic = string.Empty;

            if (matriculaView.Imagen != null)
            {
                pic = Path.GetFileName(matriculaView.Imagen.FileName);
                path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);
                matriculaView.Imagen.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    matriculaView.Imagen.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

            var matricula = db.Matriculas.Find(matriculaView.MatriculaId);

            matricula.Siglas = matriculaView.Siglas;
            matricula.Serie = matriculaView.Serie;
            matricula.AnioFabricacion = matriculaView.AnioFabricacion;
            matricula.Poseedor = matriculaView.Poseedor;
            matricula.Capacidad = matriculaView.Capacidad;
            matricula.Certificado = matriculaView.Certificado;
            matricula.ModeloId = matriculaView.ModeloId;
            if (!string.IsNullOrEmpty(pic))
            {
                matricula.Imagen = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}", pic);
            }

            ViewBag.ModeloId = new SelectList(db.Modelos, "ModeloId", "Nombre", matricula.ModeloId);
            db.Entry(matricula).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Matriculas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Matricula matricula = db.Matriculas.Find(id);
            if (matricula == null)
            {
                return HttpNotFound();
            }
            return View(matricula);
        }

        // POST: Matriculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Matricula matricula = db.Matriculas.Find(id);
            db.Matriculas.Remove(matricula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
