﻿using SolVueWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SolVueWeb.Controllers
{
    [Authorize(Roles ="Admin")]
    public class ModelosController : Controller
    {
        private SolVueContext db = new SolVueContext();
        // GET: Modelos
        [HttpGet]
        public ActionResult Index()
        {
            return View(db.Modelos.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Modelo modelo)
        {
            if (!ModelState.IsValid)
            {
                return View(modelo);
            }
            db.Modelos.Add(modelo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Se busca rgistro
            var modelo = db.Modelos.Find(id);
            if (modelo == null)
            {
                return HttpNotFound();
            }

            return View(modelo);
        }

        [HttpPost]
        public ActionResult Edit(Modelo modelo)
        {
            if (!ModelState.IsValid)
            {
                return View(modelo);
            }
            db.Entry(modelo).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var modelo = db.Modelos.Find(id);
            if (modelo == null)
            {
                return HttpNotFound();
            }

            var vista = new ModeloDetalleView
            {
                ModeloId = modelo.ModeloId,
                Nombre = modelo.Nombre,
                Matriculas = modelo.Matriculas.ToList(),
            };
            return View(vista);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var modelo = db.Modelos.Find(id);
            if (modelo == null)
            {
                return HttpNotFound();
            }
            return View(modelo);
        }

        [HttpPost]
        public ActionResult Delete(int id, Modelo modelo)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            modelo = db.Modelos.Find(id);
            if (modelo == null)
            {
                return HttpNotFound();
            }
            db.Modelos.Remove(modelo);
           try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                if(ex.InnerException!=null &&
                    ex.InnerException.InnerException!=null &&
                        ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                        {
                            ViewBag.Error = "No puede borrar el registro ya que tiene registros relacionados";
                         }
                else
                {
                    ViewBag.Error = ex.Message;
                }
                return View(modelo);
            }
            return RedirectToAction("Index");
        }

   
        [HttpGet] 
        public ActionResult ReasignarMatricula(int id)
        {
            var matricula = db.Matriculas.Find(id);
            //var mdloId = matricula.ModeloId;
            //matricula.ModeloId = 7;
            //if (matricula != null)
            //{
            //    db.Entry(matricula).State = EntityState.Modified;
            //    db.SaveChanges();
            //}
            //return RedirectToAction(string.Format("Details/{0}", mdloId));
            //var matriculaView = new MatriculaView
            var matricReasignaView = new MatricReasignaView
            {
                Siglas = matricula.Siglas,
                //Serie = matricula.Serie,
                //AnioFabricacion = matricula.AnioFabricacion,
                //Poseedor = matricula.Poseedor,
                //Capacidad = matricula.Capacidad,
                ModeloId = matricula.ModeloId,
                //Certificado = matricula.Certificado,
                MatriculaId = matricula.MatriculaId,
            };

            ViewBag.MdloId = new SelectList(db.Modelos, "ModeloId", "Nombre", matricula.ModeloId);
            ViewBag.siglas = matricula.Siglas;
            //return View(matriculaView);
            return View(matricReasignaView);


        }

 
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ReasignarMatricula(MatricReasignaView matricReasignaView)
        {
            if (!ModelState.IsValid)
            {
                return View(matricReasignaView);
            }

        ////Upload Imagen
        //string path = string.Empty;
        //string pic = string.Empty;

        //if (matricReasignaView.Imagen != null)
        //{
        //    pic = Path.GetFileName(matricReasignaView.Imagen.FileName);
        //    path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);
        //    matriculaView.Imagen.SaveAs(path);
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        matriculaView.Imagen.InputStream.CopyTo(ms);
        //        byte[] array = ms.GetBuffer();
        //    }
        //}

        var matricula = db.Matriculas.Find(matricReasignaView.MatriculaId);

            matricula.Siglas = matricReasignaView.Siglas;
            matricula.MatriculaId = matricReasignaView.MatriculaId;
            //matricula.Serie = matriculaView.Serie;
            //matricula.AnioFabricacion = matriculaView.AnioFabricacion;
            //matricula.Poseedor = matriculaView.Poseedor;
            //matricula.Capacidad = matriculaView.Capacidad;
            //matricula.Certificado = matriculaView.Certificado;
            matricula.ModeloId = matricReasignaView.ModeloId;
            //if (!string.IsNullOrEmpty(pic))
            //{
            //    matricula.Imagen = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}", pic);
            //}


            db.Entry(matricula).State = EntityState.Modified;
            db.SaveChanges();
            //Message="Se reasigno correctamente el modelo a la matricula";
            ViewBag.MdloId = new SelectList(db.Modelos, "ModeloId", "Nombre", matricula.ModeloId);
            TempData["Correcto"] = "Se asigno correctamente el modelo a la matricula";
            return RedirectToAction(string.Format("Details/{0}",matricula.ModeloId));

        }


        [HttpGet]
        public ActionResult AddMatricula(int modeloId)
        {
            ViewBag.MatriculaId = new SelectList(db.Matriculas.OrderBy(m => m.Siglas), "MatriculaId", "Siglas");
            var vista = new AddMatriculaView
            {
                ModeloId = modeloId,
               
            };
            var nombreModelo = db.Modelos.Find(vista.ModeloId);
            string nombre = nombreModelo.Nombre;
            ViewBag.NombreModelo = nombre;
            return View(vista);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddMatricula(AddMatriculaView vista)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.MatriculaId = new SelectList(db.Matriculas.OrderBy(m => m.Siglas), "MatriculaId", "Siglas");
                return View(vista);
            }

            

            var matriculaenmodelo = db.Matriculas.Select(m => m.MatriculaId == vista.MatriculaId && m.ModeloId==vista.ModeloId);
            if (matriculaenmodelo != null)
            {
                ViewBag.MatriculaId = new SelectList(db.Matriculas.OrderBy(m => m.Siglas), "MatriculaId", "Siglas");
                ViewBag.Error = "La matricula ya se encuentra registrada en este modelo";
                return View(vista);
            }

            var matricula = db.Matriculas.Find(vista.MatriculaId ) ;

            if (matricula != null)
            {
                ViewBag.MatriculaId = new SelectList(db.Matriculas.OrderBy(m => m.Siglas), "MatriculaId", "Siglas");
                db.Entry(matricula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction(string.Format("Details/{0}", vista.ModeloId));
            }

            ViewBag.MatriculaId = new SelectList(db.Matriculas.OrderBy(m => m.Siglas), "MatriculaId", "Siglas");
            return View(vista);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}