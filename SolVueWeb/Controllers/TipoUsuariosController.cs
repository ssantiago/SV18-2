﻿using SolVueWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SolVueWeb.Controllers
{
    public class TipoUsuariosController : Controller
    {
        private SolVueContext db = new SolVueContext();

        // GET: TipoUsuarios
        public ActionResult Index()
        {
            return View(db.TipoUsuarios.ToList() );
        }


        [HttpGet]
        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(TipoUsuario modelo)
        {
            if (!ModelState.IsValid)
            {
                return View(modelo);
            }
            db.TipoUsuarios.Add(modelo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Se busca registro
            var tipoUsuario = db.TipoUsuarios.Find(id);
                if (tipoUsuario == null)
            {
                return HttpNotFound();
            }
            return View(tipoUsuario);
        }

        [HttpPost]
        public ActionResult Editar(TipoUsuario tipoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return View(tipoUsuario);
            }
            db.Entry(tipoUsuario).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}