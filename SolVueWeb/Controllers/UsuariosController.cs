﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SolVueWeb.Models;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SolVueWeb.Controllers
{

    public class UsuariosController : Controller
    {
        private SolVueContext db = new SolVueContext();

        [Authorize(Roles = "User")]
        [HttpGet]
        public ActionResult MiPerfil()
        {
            var usuario = db.Usuarios
                .Where(u => u.NombreUsuario == this.User.Identity.Name)
                .FirstOrDefault();
            var vista = new UsuarioPerfilView
            {
                Nombre=usuario.Nombre,
                ApellidoPat=usuario.ApellidoPat,
                ApellidoMat=usuario.ApellidoMat,
                NombreUsuario=usuario.NombreUsuario,
                //Ficha=usuario.Ficha,
                Telefono=usuario.Telefono,
                //Extension=usuario.Extension,
                //Celular=usuario.Celular,
                //DependenciaId=usuario.DependenciaId,
                //Cargo=usuario.Cargo,
                Foto=usuario.Foto,
                UsuarioId = usuario.UsuarioId,

            };
            return View(vista);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MiPerfil(UsuarioPerfilView vista )
        {
            if (ModelState.IsValid)
            {
                //Upload
                string path = string.Empty;
                string pic = string.Empty;

                if (vista.NuevaFoto != null)
                {
                    pic = Path.GetFileName(vista.NuevaFoto.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);

                    vista.NuevaFoto.SaveAs(path); //se guarda la foto local

                    using (MemoryStream ms = new MemoryStream()) //se guarda en el servidor la foto
                    {
                        vista.NuevaFoto.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
                var usuario = db.Usuarios.Find(vista.UsuarioId);

                usuario.Nombre = vista.Nombre;
                usuario.ApellidoPat = vista.ApellidoPat;
                usuario.ApellidoMat = vista.ApellidoMat;
                //usuario.Ficha = vista.Ficha;
                usuario.Telefono = vista.Telefono;
                //usuario.Extension = vista.Extension;
                //usuario.Celular = vista.Celular;
                //usuario.DependenciaId = vista.DependenciaId;
                //usuario.Cargo = vista.Cargo;
                //if (!string.IsNullOrEmpty(pic))
                {
                    usuario.Foto = string.Format("~/Content/Fotos/{0}", pic);
                }

                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Home");

            }
            return View(vista);
        }



        [Authorize(Roles = "Admin")]
        // GET: Usuarios
        public ActionResult Index()
        {
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var usuarios = db.Usuarios.ToList();
            var usuariosView = new List<UsuarioIndexView>();

            foreach (var usuario in usuarios)
            {
                var usuarioASP = userManager.FindByEmail(usuario.NombreUsuario);
                usuariosView.Add(new UsuarioIndexView
                {

                Nombre = usuario.Nombre,
                    ApellidoPat = usuario.ApellidoPat,
                    ApellidoMat = usuario.ApellidoMat,
                    NombreUsuario = usuario.NombreUsuario,
                    //Ficha = usuario.Ficha,
                    Telefono = usuario.Telefono,
                    //Extension = usuario.Extension,
                    //Celular = usuario.Celular,
                    ////DependenciaId = usuario.DependenciaId,
                    //Cargo = usuario.Cargo,
                    Foto = usuario.Foto,
                    EsAdmin = usuarioASP != null && userManager.IsInRole(usuarioASP.Id , "Admin"),
                    UsuarioId=usuario.UsuarioId,           
                });
             }

            return View(usuariosView);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult OnOffAdmin(int id)
        {
            var usuario = db.Usuarios.Find(id);
            if (usuario != null)
            {
                var userContext = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
                var usuarioASP = userManager.FindByEmail(usuario.NombreUsuario);

                if (usuarioASP != null)
                {
                    if (userManager.IsInRole(usuarioASP.Id, "Admin"))
                    {
                        userManager.RemoveFromRole(usuarioASP.Id, "Admin");
                    }
                    else
                    {
                        userManager.AddToRole(usuarioASP.Id, "Admin");
                   }
                }
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }


            return View(usuario);
        }


        [Authorize(Roles = "Admin")]
        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsuarioView UsuarioView)
        {
            if (!ModelState.IsValid)
            {
                return View(UsuarioView);
            }

            //Upload
            string path = string.Empty;
            string pic = string.Empty;

            if (UsuarioView.Foto != null)
            {
                pic = Path.GetFileName(UsuarioView.Foto.FileName);
                path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);

                UsuarioView.Foto.SaveAs(path); //se guarda la foto local

                using (MemoryStream ms = new MemoryStream()) //se guarda en el servidor la foto
                {
                    UsuarioView.Foto.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                } 
            }

            //Guarda el registro
            var usuario = new Usuario
            {
                Nombre = UsuarioView.Nombre,
                ApellidoPat = UsuarioView.ApellidoPat,
                ApellidoMat = UsuarioView.ApellidoMat,
                NombreUsuario = UsuarioView.NombreUsuario,
                //Ficha = UsuarioView.Ficha,
                Telefono = UsuarioView.Telefono,
                //Extension = UsuarioView.Extension,
                //Celular = UsuarioView.Celular,
                //DependenciaId = UsuarioView.DependenciaId,
                //Cargo = UsuarioView.Cargo,
                //Foto = pic == string.Empty ? string.Empty : string.Format("~/Content/Fotos/{0}", pic)

            };

            db.Usuarios.Add(usuario);

            try
            {
                db.SaveChanges();
                this.CreateASPUser(UsuarioView);

            }
            catch (Exception ex)
            {
                if(ex.InnerException!=null && 
                    ex.InnerException.InnerException!=null && 
                    ex.InnerException.InnerException.Message.Contains("NombreUsuarioIndex"))
                {
                    ViewBag.Error = "El correo ya ha sido utilizado para otro usuario";
                }
                else
                {
                    ViewBag.Error = ex.Message;
                }
                return View(UsuarioView);
            }
       
            return RedirectToAction("Index");

           
        }

        private void CreateASPUser(UsuarioView usuarioView)
        {
            //Administracion de Usuarios
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            //Crear Role de usuarios
            string roleName = "User";

            //Se verifica que exista el rol, si no existe:
            if (!roleManager.RoleExists(roleName))
            {
               roleManager.Create(new IdentityRole(roleName));
            }

            //Creacion del usuario ASPNet
            var userASP = new ApplicationUser
            {
                UserName = usuarioView.NombreUsuario,
                Email = usuarioView.NombreUsuario,
                PhoneNumber = usuarioView.Telefono,
            };
            userManager.Create(userASP, userASP.UserName);

            //Se agrega usuario al rol
            userASP = userManager.FindByName(usuarioView.NombreUsuario);
            userManager.AddToRole(userASP.Id, "User");

        }

        [Authorize(Roles = "Admin")]
        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }

            var usuarioView = new UsuarioView()
            {
                Nombre = usuario.Nombre,
                ApellidoPat = usuario.ApellidoPat,
                ApellidoMat = usuario.ApellidoMat,
                //Ficha = usuario.Ficha,
                Telefono = usuario.Telefono,
                //Extension = usuario.Extension,
                //Celular = usuario.Celular,
                //DependenciaId = usuario.DependenciaId,
                //Cargo = usuario.Cargo,
                UsuarioId=usuario.UsuarioId,
                NombreUsuario=usuario.NombreUsuario,
            };

            return View(usuarioView);
        }

        // POST: Usuarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsuarioView UsuarioView)
        {
            if (!ModelState.IsValid)
            {
                return View(UsuarioView);
            }

            //Upload
            string path = string.Empty;
            string pic = string.Empty;

            if (UsuarioView.Foto != null)
            {
                pic = Path.GetFileName(UsuarioView.Foto.FileName);
                path = Path.Combine(Server.MapPath("~/Content/Fotos"), pic);

                UsuarioView.Foto.SaveAs(path); //se guarda la foto local

                using (MemoryStream ms = new MemoryStream()) //se guarda en el servidor la foto
                {
                    UsuarioView.Foto.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }
            var usuario = db.Usuarios.Find(UsuarioView.UsuarioId);

            usuario.Nombre = UsuarioView.Nombre;
            usuario.ApellidoPat = UsuarioView.ApellidoPat;
            usuario.ApellidoMat = UsuarioView.ApellidoMat;
            //usuario.Ficha = UsuarioView.Ficha;
            usuario.Telefono = UsuarioView.Telefono;
            //usuario.Extension = UsuarioView.Extension;
            //usuario.Celular = UsuarioView.Celular;
            //usuario.DependenciaId = UsuarioView.DependenciaId;
            //usuario.Cargo = UsuarioView.Cargo;
            if (!string.IsNullOrEmpty(pic))
            {
                usuario.Foto = string.Format("~/Content/Fotos/{0}", pic);
            }

            db.Entry(usuario).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);
            db.Usuarios.Remove(usuario);
            try
            {
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                if (ex.InnerException != null
                    && ex.InnerException.InnerException != null
                    && ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(
                        string.Empty,
                        "El registro no pude ser eliminado por tener registros relacionados");
                }
                else
                {
                    ModelState.AddModelError(
                         string.Empty,
                         ex.Message);

                }
                return View(usuario);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
