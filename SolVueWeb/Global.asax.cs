﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SolVueWeb.Migrations;
using SolVueWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SolVueWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SolVueContext, Configuration>());
            this.CheckSuperuser();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void CheckSuperuser()
        {
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var db = new SolVueContext();

            this.CheckRole("Admin", userContext);
            this.CheckRole("User", userContext);
            var user = db.Usuarios
                .Where(u => u.NombreUsuario.ToLower().Equals("sergiosantiago1@hotmail.com")).FirstOrDefault();
            if (user == null)
            {
                 user = new Usuario
                {
                    Nombre = "Sergio",
                    ApellidoPat = "Santiago",
                    ApellidoMat = "Gonzalez",
                    //Ficha = 139151,
                    Telefono = "9232332377",
                    //Celular = "9232389192",
                    NombreUsuario = "sergiosantiago1@hotmail.com",
                    Foto= "~/Content/Fotos/SSG.jpg",
                    
                 };

                db.Usuarios.Add(user);
                db.SaveChanges();
            }

            var userASP = userManager.FindByEmail( user.NombreUsuario);
            if (userASP == null)
            {
                userASP = new ApplicationUser
                {
                    UserName = user.NombreUsuario,
                    Email = user.NombreUsuario,
                    PhoneNumber = user.Telefono,                  
                };
                userManager.Create(userASP,"12345678");
            }
            userManager.AddToRole(userASP.Id, "Admin");
            userManager.AddToRole(userASP.Id, "User");
        }

        private void CheckRole(string roleName, ApplicationDbContext userContext)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            //Se verifica que exista el rol, si no existe:
            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }

        }
    }
}
