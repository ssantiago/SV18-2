namespace SolVueWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20170102 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Modeloes", "Nombre", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Modeloes", "Nombre", c => c.String());
        }
    }
}
