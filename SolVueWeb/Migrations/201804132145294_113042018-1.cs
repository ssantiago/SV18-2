namespace SolVueWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1130420181 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Matriculas", "Certificado", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Matriculas", "Certificado", c => c.String(maxLength: 50));
        }
    }
}
