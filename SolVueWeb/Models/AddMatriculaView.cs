﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class AddMatriculaView
    {
        public int ModeloId { get; set; }
        public int MatriculaId { get; set; }
        public string Siglas { get; set; }
    }
}