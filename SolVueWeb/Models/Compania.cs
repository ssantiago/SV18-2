﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class Compania
    {
        [Key]
        public int CompaniaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(50, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 5)]
        public string Nombre { get; set; }

        public string Direccion { get; set; }
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(10, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 2)]
        public string Siglas { get; set; }
    }
}