﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class Contrato
    {
        [Key]
        public int ContratoId { get; set; }

        [StringLength(20, ErrorMessage = "El campo {0} debe cotener un maximo de {1} y un minnimo de {2} caracteres", MinimumLength = 10)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public string Numero { get; set; }

        [DataType(DataType.Date)]
        [Display(Name ="Fecha de Inicio")]
        [DisplayFormat(DataFormatString ="{0:yyyy-mm-dd}",ApplyFormatInEditMode =true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Termino")]
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime FechaFin { get; set; }

        [Display(Name = "Monto Maximo")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal MontoMax { get; set; }

        [Display(Name = "Compañia")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int CompaniaId { get; set; }

        public int PresupuestoId { get; set; }

    }
}