﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class MatricReasignaView
    {
        public int MatriculaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(30)]
        [Display(Name = "Matrícula")]
        public string Siglas { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public int ModeloId { get; set; }

    }
}