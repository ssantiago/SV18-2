﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class Matricula
    {
        [Key]
        public int MatriculaId { get; set; }

        [Required(ErrorMessage ="El campo {0} es requerido")]
        [StringLength(30)]
        [Display(Name ="Matrícula")]
        public string Siglas { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(30)]
        public string Serie { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Año de fabricación")]
        public int AnioFabricacion { get; set; }

        [StringLength(50)]
        public string Poseedor { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Capacidad pax")]
        public int Capacidad { get; set; }

         public int ModeloId { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Cert. de matricula")]
        public string Certificado { get; set; }

        public string Imagen { get; set; }
        public virtual Modelo Modelo { get; set; }

    }
}