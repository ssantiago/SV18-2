﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class ModeloDetalleView
    {
        public int ModeloId { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(50, ErrorMessage = "El campo {0} debe cotener un maximo de {1} y un minnimo de {2} caracteres", MinimumLength = 3)]
        public string Nombre { get; set; }
        public List<Matricula> Matriculas { get; set; }

    }
}