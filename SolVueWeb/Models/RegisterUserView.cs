﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class RegisterUserView:UsuarioView
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Password")]
        [StringLength(20, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Confirm Password")]
        [StringLength(20, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="El Pasword y la Confirmacion no son iguales")]
        public string ConfirmPassword { get; set; }

    }
}