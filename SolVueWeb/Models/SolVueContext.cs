﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class SolVueContext:DbContext
    {
        public SolVueContext():base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Modelo> Modelos { get; set; }

        public DbSet<Matricula> Matriculas { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Contrato> Contratos { get; set; }

        public DbSet<Compania> Companias { get; set; }

        public DbSet<TipoUsuario>TipoUsuarios { get; set; }


    }
}