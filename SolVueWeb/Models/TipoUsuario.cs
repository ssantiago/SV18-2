﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class TipoUsuario
    {
        public int TipoUsuarioId { get; set; }
        public string Descripcion  { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}