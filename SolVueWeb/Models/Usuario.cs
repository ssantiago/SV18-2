﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    public class Usuario
    {
        [Key]
        public int UsuarioId { get; set; }

        [Required(ErrorMessage ="El campo {0} es requerido")]
        [StringLength(50, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 2)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(50, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 2)]
        [Display(Name ="Apellido Paterno")]
        public string ApellidoPat { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(50, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Apellido Materno")]
        public string ApellidoMat { get; set; }

        [Index("NombreUsuarioIndex", IsUnique=true)]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name ="E-Mail")]
        [StringLength(100, ErrorMessage ="El campo {0} debe contener maximo {1} y minimo {2} caracteres",MinimumLength =7)]
        [DataType(DataType.EmailAddress)]
        public string NombreUsuario { get; set; }

        //public int Ficha { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(20, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 7)]
        public string Telefono { get; set; }

        //[StringLength(20, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 7)]
        //public string Extension { get; set; }

        //[StringLength(20, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 7)]
        //public string Celular { get; set; }

        ////public int DependenciaId { get; set; }

        //[StringLength(50, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 10)]
        //public string Cargo { get; set; }

        [DataType(DataType.ImageUrl)]
        [StringLength(200, ErrorMessage = "El campo {0} debe contener maximo {1} y minimo {2} caracteres", MinimumLength = 5)]
        public string Foto { get; set; }

        public virtual TipoUsuario TipoUsuario { get; set; }
    }
}