﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    [NotMapped]
    public class UsuarioIndexView:Usuario
    {

        [Display(Name = "Es Admin?")]
        public bool EsAdmin { get; set; }
    }
}