﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SolVueWeb.Models
{
    [NotMapped]
    public class UsuarioPerfilView:Usuario
    {
        [Display(Name="Nueva Foto")]
        public HttpPostedFileBase NuevaFoto { get; set; }

    }
}