﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolVueWeb.Startup))]
namespace SolVueWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
